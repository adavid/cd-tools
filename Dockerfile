FROM gitlab-registry.cern.ch/ci-tools/ci-web-deployer

RUN yum install -y rpm-build && yum clean all

COPY bin/ /sbin/
