## SUMMARY
A new docker image to use to build rpm(s) and update meta-data of a repository based on eos: gitlab-registry.cern.ch/ilarbi/cd-tools .
This image is built from gitlab.cern.ch/ci-tools/ci-web-deployer image, so you can also use it to deploy files (eg. rpms) to eos with the script *deploy-eos*.

## IMAGE SOURCE
Concerning the "deploy-eos" command you can see the README of  https://gitlab.cern.ch/ci-tools/ci-web-deployer. 
There are only two variables that you need to define in CI/CD settings, as for ci-web-deployer image, which are required to run
"deploy-eos" and "updaterepo" commands: 
`EOS_ACCOUNT_PASSWORD`
`EOS_ACCOUNT_USERNAME`

## NEW SCRIPTS

There are two new scripts: "updaterepo" and "create_packages"

* *updaterepo </path/of/root/repository>*:

It's a command which connect to lxplus service on EOS_ACCOUNT_USERNAME with EOS_ACCOUNT_PASSWORD to run the command createrepo 
in *</path/of/root/repository>*: "cd /path/of/root/repository && createrepo --update --database ." There is no option avalaible yet.
So, You just need to give one argument < /path/of/root/repository> and then the script will run the previous command in this 
directory to update the xml files of repodata directory. For more information on createrepo : https://linux.die.net/man/8/createrepo .


* *create_packages [ -R | -B | -RB ] </path/to/name_of_artifact(s)> </path/to/install/files>*:

variable:
CI_OUTPUT_DIR (optional): The variable is used to stock rpms built, which will be deployed to eos with command deploy-eos, in CI_OUT_DIR directory temporarily.
By default "public"

There are 3 different uses of the command.
1. Without option: The script will package every files recursively of </path/to/name_of_artifact(s)> directory into a unique 
rpm.
The rpm name is "name_of_artifact(s)", the version number is the current date YEAR.MONTH.DAY.HOUR.MIN.SECOND and 
the release number is the commit_ID of gitlab. The rpm will install files in </path/to/install/files> directory on the client computer.

2. -R option will create a unique rpm for each subdirectory of  </path/to/name_of_artifact(s)> with the same logic.
Each rpm will have the basename of its subdirectory.
The different file tree of each rpms created will be installed in  </path/to/install/files> directory on the client computer.

3. -B option will create  a "global" rpm named "name_of_artifact(s)" for each rpms contained in </path/to/name_of_artifact(s)> and places every rpms in CI_OUTPUT_DIR.

4. -RB option is same exactly same that -R but it will also create a "global" rpm named "name_of_artifact(s)" which only    
has the dependencies of each unique rpm built. It's a way to install every file tree by only installing this 
package.

## AFTER USING COMMANDS

To install rpm on the machine first you have to add the repository to yum repositories by adding a file .repo in /etc/yum.repos.
d/. This help could be useful : https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/sec-managing_yum_repositories .

Furthermore if you want you can add a line in my_new_repo.repo file e.g.: "metadata_expire=10m" to update yum cache of the 
repository if the last use of it is greater than 10 minutes.

The destination where artefacts will be installed on the machine when user will launch yum install "name_of_artifact(s)" is /path/to/install/files 
For example if /path/to/install/files="/opt/cms-hgcal-firmware/hgc-test-systems", the result will be : 
```
/
└── opt
    └── cms-hgcal-firmware
        └── hgc-test-systems
            ├── <name_of_1st_rpm>
            │   ├── bit
            │   ├── dtbo
            │   └── xml
            ├── <name_of_2nd_rpm>
            │   ├── bit
            │   ├── dtbo
            │   └── xml
            └── <name_of_3rd_rpm>
                ├── bit
                ├── dtbo
                └── xml
```
